build:
	rm -rf public/ && hugo

serve:
	hugo server -v -D --disableFastRender

deploy:
	./deploy.sh
