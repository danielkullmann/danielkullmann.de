# My personal, professional home page

This is the source code for my home page on https://danielkullmann.de.

It is built using [hugo](http://gohugo.io).

To deploy it, I need to run deploy.sh, having first set FTP_USER and FTP_PASSWORD in the ` .secrets` file.

 