#!/bin/sh

set -eu

if test -f ./.secrets; then
  . ./.secrets
fi

rm -rf public
hugo -v

cd public 

ncftpput -R -v -u "$FTP_USER" -p "$FTP_PASSWORD" web11.biohost.de /Sites/danielkullmann.de/ .

