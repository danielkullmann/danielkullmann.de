---
title: "Persönliche Projekte"
date: 2023-09-11T12:00:00+02:00
---

Dies ist eine Liste meiner persönlichen Projekte.

## [simple-offline-map](https://github.com/daniel-kullmann/simple-offline-map)

Eine einfache Kartenanwendung, basierend auf leaflet.js und OpenStreetMap-Daten.
Die Hauptfeatures sind:

* Offline Cachen der Kartenkacheln
* Anzeige von GPX Tracks
* Manuelles Erstellen von GPX tracks

Ich will diese Anwendung zu einer vollständigen GPX-Track-Analyse-Anwendung erweitern;
das offline Cachen ist nicht mehr so wichtig für mich.

## [corona](https://gitlab.com/danielkullmann/corona)

Ein sehr simple Webanwendung, die mit Hilfe von Chart.js verschiedene Grafiken von Coronadaten anzeigt:
für verschiedene auswählbare Länder wird die Entwicklung von Fall- und Todesdaten angezeigt.

Die Anwendung wird mit Hilfe von Gitlab CI automatisch nach https://danielkullmann.gitlab.io/corona/ deployt.

## [cyclingstats](https://gitlab.com/danielkullmann/cyclingstats)

Diese Projekt enthält einen Web Scraper für Daten von Radrennen, die von https://procyclingstats.com geholt werden, und eine einfache Webanwendung die verschiedene Arten von Auswertungen anzeigt.

Die Anwendung wird mit Hilfe von Gitlab CI automatisch nach https://danielkullmann.gitlab.io/cyclingstats/ deployt.

## [vendeeglobe2020](https://gitlab.com/danielkullmann/vendeeglobe2020)

Ähnlich wie bei cyclingstats enthält auch dieses Projekteinen Web Scraper für Daten des Segelrennens Vendée Globe, und eine einfache Webanwendung die diese Daten in Graphen anzeigt.

Die Anwendung wird mit Hilfe von Gitlab CI automatisch nach https://danielkullmann.gitlab.io/vendeeglobe2020/ deployt.
