---
title: "Personal Projects"
date: 2023-09-11T12:00:00+02:00
---

This is a list of my personal projects.

## [simple-offline-map](https://github.com/daniel-kullmann/simple-offline-map)

This is a simple map application, based on leaflet.js. Its main features are:

* Offline caching of map tiles
* Display of GPX tracks
* Manual creation of GPX tracks

The idea is to extend it to a full-fledged track analysis application; the offline
caching is not so important for me anymore.

## [corona](https://gitlab.com/danielkullmann/corona)

A very simple web application that shows several plots of corona virus data:
for several, selectable countries, the development of the number of cases and deaths can
be visualized.

The web application is automatically deployed via Gitlab CI to https://danielkullmann.gitlab.io/corona/.

## [cyclingstats](https://gitlab.com/danielkullmann/cyclingstats)

This project contains a web scraper for bicycle race data that targets https://procyclingstats.com, plus a very simple web application for showing certain kinds of visualisations of bike race progression for multi-stage races like the Tour de France.

The web application is automatically deployed via Gitlab CI to https://danielkullmann.gitlab.io/cyclingstats/.

## [vendeeglobe2020](https://gitlab.com/danielkullmann/vendeeglobe2020)

Similar to cyclingstats, this project contains a web scraper for race data for the sailing race Vendée Globe, plus a very simple web application for showing that data in charts.

The web application is automatically deployed via Gitlab CI to https://danielkullmann.gitlab.io/vendeeglobe2020/.
