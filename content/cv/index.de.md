---
title: "Lebenslauf"
date: 2023-09-11T12:00:00+02:00
---

## Juli 2019 - jetzt

* Freiberuflicher Entwickler
* Entwicklung von Webanwendungen:
  * SAPUI5 Anwendungen auf der SAP Cloud Platform (BTP)
  * Java Services
  * Python Services
* Programmiersprachen: JavaScript, Java, Python, Bash
* Ausgewählte Tools und Technologien: Spring Boot, Django,  Agile
  Softwareentwicklung (Scrum/JIRA), Maven, Shell scripts, Git, Gitlab, PostgreSQL,
  SAP Cloud Platform, SAPUI5, SAP HANA

## Oktober 2019 - jetzt

* Expert Karat Interviewer
* Karat (freiberuflich, Teilzeit)
* Durchführung von technischen Interviews für andere Firmen
* Technische Interviews bestehen aus Wissensfragen zu einem bestimmten Thema, einer Projektdiskussion und Programmieraufgaben

## Oktober 2013 - September 2018

* Senior Developer
* SAP Innovation Center in Potsdam
* Verschiedene Projekte:
  * Erstellen eines Virtual Data Models (VDM) für das RE-FX ERP Modul (Immobilien) von SAP
  * Entwicklung einer Anwendung zur Analyse von Finanzdaten, basierend auf XSJS (HANA serverseitiges JavaScript)
  * Konversion der Anwendung auf Scala und die HANA Cloud Platform
  * Mehrere kleiner Projekte, unter anderem Untersuchung der Performance von verschiedenen Ansätzen
* Programmiersprachen: JavaScript (HANA XSJS, Node.js und Browser), Java, Python, Scala, Bash
* Tools und Technologien: SAP Cloud Platform, SAP HANA, SAPUI5/OpenUI5, Web Services, Git, Jira, Eclipse, IntelliJ IDEA, Travis, Jenkins
      
## Mai 2012 - Juli 2013

* Wissenschaftlicher Assistent
* Technische Universität Dänemark
* Weiterentwicklung und Wartung der existierenden Software zur Steuerung des experimentellen Stromnetzes SYSLAB
* Administration der mehr als 30 SYSLAB Server
* Programmiersprachen: Java, Python, C/C++, Bash
* Tools und Technologien: PostgreSQL, Web Services, Mercurial, Eclipse, Continuous Integration, Gentoo Linux, Ubuntu Linux

## September 2008 - April 2012

* PhD
* Technische Universität Dänemark
* Bereich: Energietechnik/Informatik
* Titel: Policy-based Communication for the Control of Power Systems
* Evaluierung eines neuen Konzeptes für die Kontrolle von Stromnetzen
* Entwicklung einer Java-Bibliothek, mehrerer Experimenteu und Simulationen
* Weiterentwicklung der SYSLAB Plattform
* Programmiersprachen: Java, Python, C/C++
* Tools und Technologien: JADE, Regelsysteme (JBoss Drools), Mercurial, Eclipse, Gentoo Linux, Ubuntu Linux, IEC 61850, IEC 61970 (CIM)

## November 2007 - Juli 2008

* Senior Software Developer
* Zakoa GmbH, Frankfurt
* Projektleitung und Entwicklung von mehreren Webanwendungen unter Verwendung von PHP und JavaScript
* Verantwortlich für die Wartung einer Java basierten Webanwendung
* Programmieraufgaben: Java, PHP, JavaScript
* Tools und Technologien: MySQL, Zend Framework, Eclipse, Subversion, jQuery, Linux, Windows XP

## Januar 2007 - Oktober 2007

* Sabbatjahr
* Reise durch Indien, Europa, und Kalifornien
* Freiwillige Arbeit

## September 2002 - Dezember 2006

* Software Developer
* 5POINT AG, Darmstadt
* Projektleitung, Entwicklung und Wartung mehrerer Webanwendungen in Java und Perl
* Programmiersprachen: Java, Perl, JavaScript, C
* Tools und Technologien: Tomcat, PostgreSQL, MySQL, Struts, Eclipse, Subversion, jQuery, Linux, Windows 2000


## Juni 2001 - Dezember 2001

* Diplomarbeit für mein Diplom in Informatik
* Fraunhofer CRCG Institute in Providence, Rhode Island
* Titel: Intelligent Behavior Control and World Modeling for Virtual Humans in Virtual Environments


## November 1998 - Juni 2001

* Wissenschaftliche Hilfskraft
* Institut für Computergrafik IGD, Technische Universität Darmstadt 
* Entwicklung einer Bibliothek und GUI-Anwendung in C++, um auf 3D Eingabegeräte zuzugreifen und sie zu konfigurieren 
* Programmiersprache: C++
* Tools und Technologien: OpenGL, Qt

## September 1994 - April 2002

* Studium der Diplominformatik
* Technische Universität Darmstadt
* Schwerpunkte: Software Engineering, Datenbanken, Embedded Systeme, Kryptographie, Computernetzwerke
