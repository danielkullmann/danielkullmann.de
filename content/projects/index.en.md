---
title: "Work Projects"
date: 2023-09-11T12:00:00+02:00
---

## Python Data Synchronisation Project

An existing system with various micro-services was extended by a new micro-service that
synchronizes certain data with an external system. The synchronization is triggered by
certain events. The triggering mechanism uses AWS SNS topics and SQS queues. The data
is sent to the external system through a RabbitMQ queue.

* Language: Python
* Framework: Tornado
* Deployment: Docker, Hashicorp Nomad, AWS, Hetzner Cloud

## Java/Kotlin/Python Micro Services

A number of microservices were implemented for an IDP (Intelligent Document Processing) platform.
The data pipeline was not using the full possibilites of concurrent processing, and
several changes to the main micorservice and extensive debugging lead to a significant performance
increase.

* Languages: Java, Kotlin, Python
* Frameworks: Spring Boot, FastAPI
* OCR, image pre-processing, converting of data formats
* debugging, performance improvements

## SAP UI5 BTP Applications

A number of applications were implemented, using SAPUI5 on the frontend and Java and OData servives on the backend.
The applications were deployed on the SAP BTP cloud (dev, test, anfd prod environments).

* Languages: JavaScript, Java
* Frameworks: SAPUI5, Spring Boot
* Data sources: SAP HANA DB, OData Services (to access SAP ERP data) 
* Deployment on the Business Technology Platform (SAP BTP)

## Python/Django: Management of used cars

An already Python/Django application was extended by several features. Several requests were originally very slow;
these requests were sped up significantly.

* Python/Django
* Vue.js
* Database: PostgreSQL
* multiple CSV imports
* PDF export
* Support for multi tenancy
* Deployment on AWS

## Python/Django: Platform for generation report cards

An existing Python/Django application that was used to generate school report cards was extended by several features, and several probelms were debugged and fixed.
Additionaly, the move to multi tenancy was deplyoment on Azure was supported.

* Python/Django
* React.js
* Database: PostgreSQL
* Deployment on Azure
* RabbitMQ

## Python/Django: Planning of renovations

An existing application was extended by several features.

* Python/Django
* React.js
* Database: PostgreSQL
* Deployment on AWS
* Multiple CSV imports
* RabbitMQ
* Charts for the analysis of performance

## Mendix: PDF creation

A PDF export was developed for the Low-Code platform Mendix.

* Mendix
* Java
* PDF export
