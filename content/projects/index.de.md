---
title: "Arbeitsprojekte"
date: 2023-09-11T12:00:00+02:00
---

## Python Data Synchronisation Projekt

Ein existierendes System aus mehreren Microservices wurde um einen neuen Microservice erweitert.
Dieser synchronisierte bestimmte Datensätze mit einem externen System; diese Synchronisation wurde
durch verscheidene Ereignisse ausgelöst. Der Triggermechanismus benutzte AWS SNS Topics und SQS Queues.
Die zu synchronisierenden Datensätze wurden pr RabbitMQ an das externe System gesendet.

* Involvierte Sprache: Python
* Framework: Tornado
* Deployment: Docker, Hashicorp Nomad, AWS, Hetzner Cloud

## Java/Kotlin/Python Microservices

Für eine IDP (Intelligent Document Processing) Plattform wurden eine Reihe von Micro Services neu entwickelt bzw. gepflegt.
Die Data Pipeline nutzte nicht die ganzen Möglichkeiten Nebenläufigkeit. Deshalb wurde der Haupt-Microservice geändert
und nach einigem Debugging ergab sich eine signifikante Erhöhung der Performance.

* Involvierte Sprachen: Java, Kotlin, Python
* Frameworks: Spring Boot, FastAPI
* OCR, Image Preprocessing, Konvertierung von Datenformaten 
* Debugging und Performanceverbesserungen

## SAP UI5 BTP Anwendungen

Eine Reihe von Anwendungen wurden mit SAPUI5 auf dem Frontend und einigen Java Services bzw. OData Services auf
dem Backend implementiert. Die Anwendungen wurden auf der SAP BTP Cloud deployed (dev, test und prod Umgebungen)

* Involvierte Sprachen: JavaScript, Java
* Frameworks: SAPUI5, Spring Boot
* Datenquellen: SAP HANA DB, OData Services (zum Auslesen von SAP ERP Daten) 
* Deployment auf der Business Technology Platform (SAP BTP)

## Python/Django: Verwaltung von Gebrauchtfahrzeugen

Dies war eine schon existierende Python/Django Anwendung, die um mehrere Features erweitert wurde. Einige Requests
waren ursprünglich sehr langsam; hier wurde die Performance deutlich verbessert.

* Python/Django
* Vue.js
* Datenbank: PostgreSQL
* mehrere CSV-Imports
* PDF Export
* Multi-Tenant Unterstützung
* Deployment auf AWS
* Debugging und Performanceverbesserungen

## Python/Django: Zeugnisplattform

Eine existierende Python/Django Anwendung zum Erzeugen von Schulzeugnissen wurde um verschiedene Features erweitert; einige Probleme wurden debugged.
Ausserdem wurde die Umstellung auf Multi-Tenant und Deployment auf Azure begleitet.

* Python/Django
* React.js
* Datenbank: PostgreSQL
* Deployment auf Azure
* RabbitMQ für das Erzeugen der Zeugnisse

## Python/Django: Planungsanwendung für Wohnungsrenovierungen

Dies ist eine bereits existierende Anwendung, die um einige Features erweitert wurde.

* Python/Django
* React.js
* Datenbank: PostgreSQL
* Deployment auf AWS
* Mehrere CSV-Importe
* Analyse-Charts

## Mendix: PDF Erzeugung

Für die Low-Code-Plattform Mendix wurde in Java ein PDF Export entwickelt.

* Mendix
* Java
* PDF Export
