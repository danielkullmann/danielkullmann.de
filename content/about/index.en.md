---
title: "About Me"
date: 2023-09-11T12:00:00+02:00
---

I am an experienced software developer, working as a freelancer since 2019. Before that, I worked in various small and large companies; the latest company I worked for was SAP. Due to that, I have a certain affinity 
towards the modern SAP technologies: SAP HANA, SAPUI5, the SAP Cloud Platform, and the programming languages Java and JavaScript. Other technologies that I have a lot of experience with are Python (with e.g. Django or FastAPI), Kotlin, Scrapy, Scala and DevOps technologies like Docker, Kubernetes, Helm and several CI/CD environments.

## Core Competencies

* Software development in Python
  * Web applications with e.g. Django or FastAPI
  * Automation of processes
  * Extraction of information, e.g. from web sites (with Scrapy) or from Excel, CSV or PDF files
  * Export of data into e.g. Excel, CSV, PDF
  
* Software Development in Java
  * with Spring or Spring/Boot frameworks
  * Integration of external services, e.g. e-mail, OData, ReST services
  
* Development of business applications on SAP BTP (formerly SAP Cloud Platform)
  * JavaScript on the frontend and Java or OData services as backend
  * SAPUI5 as frontend framework
  * Deployment on the SAP BTP (Business Technology Platform)

* DevOps
  * Docker
  * Kubernetes
  * Hashicorp Nomad
  * Helm
  * Jenkins
  * Gitlab CI/CD
  * AWS, Azure, SAP BTP

In all mentioned areas, I cover all aspects from new development, feature enhancements and debugging to deployment.

## Social/Business Networks

* Linkedin: https://www.linkedin.com/in/daniel-kullmann-97622926/
* Xing: https://www.xing.com/profile/Daniel_Kullmann2/
* freelance.de: https://www.freelance.de/Freiberufler/159128 
* [Toptal](https://toptal.com): I am a Toptal engineer
* [Karat](https://karat.io): I am a Karat Expert Interview Engineer

## Networks with a technical Focus

* Stackoverflow: https://stackoverflow.com/users/85615/daniel-kullmann
* Unix/Linux Stackexchange: https://unix.stackexchange.com/users/10788/daniel-kullmann
* Github: https://github.com/daniel-kullmann/
* Gitlab:  https://gitlab.com/danielkullmann/
