---
title: "Über mich"
date: 2023-09-11T12:00:00+02:00
---

Ich bin ein erfahrener und passionierter Softwareentwickler und seit 2019 selbständig. Ich arbeite meistens mit den Sprachen Java, Python und JavaScript. Im Backend fühle ich mich besonders zuhause,
habe aber auch immer mit DevOps- und Frontendtechnologien gearbeitet. Meine große Stärke ist die Fehlersuche, besonders durch
mehrere Schichten oder auch in verteilten Systemen, und das Schaffen von Tools, um bestimmte Aufgaben zu automatisieren und so
meine Arbeit zu verbessern und zu beschleunigen.

Ich habe ein paar Jahre bei SAP gearbeitet, deshalb habe ich eine gewisse Affinität zu den moderneren SAP-Technologien: SAP HANA, SAPUI5, die SAP BTP Cloud und als Programmiersprachen Java und JavaScript. Andere Technologien, mit denen ich viel arbeite, sind Python (mit z.B. Django oder FastAPI), Scrapy, Scala und DevOps Technologien wie Docker, Kubernetes, Helm und verschiedene CI/CD Umgebungen.

## Kernkompetenzen

* Softwareentwicklung mit Python
  * Webanwendungen mit z.B. Django oder FastAPI
  * Automatisierung von Prozessen
  * Extraktion von Informationen, z.B. von Webseiten (evtl. mit Scrapy), aus Exceldateien, CSV-Dateien oder PDFs
  * Export von Daten in z.B. Exceldateien, CSV-Dateien oder PDFs

* Softwareentwicklung mit Java
  * mit den Spring oder Spring/Boot Frameworks
  * Integration von externen Diensten, z.B. e-Mail, OData, oder ReST Services
  
* Entwicklung von Businessanwendungen auf der SAP BTP (ehemals SAP Cloud Platform)
  * JavaScript im Frontend und Java oder OData-Services als Backend
  * SAPUI5 als Frontend
  * Deployment auf der SAP BTP (Business Technology Platform)

* DevOps
  * Docker
  * Kubernetes
  * Hashicorp Nomad
  * Helm
  * Jenkins
  * Gitlab CI/CD
  * AWS, Azure, SAP BTP

In allen diesen Feldern biete ich die Neuentwicklung als auch Weiterentwicklung von Anwendungen an,
die Wartung von bestehenden Systemen, und die Fehlersuche und -behebung.

## Soziale/Business Netzwerke

* Linkedin: https://www.linkedin.com/in/daniel-kullmann-97622926/
* Xing: https://www.xing.com/profile/Daniel_Kullmann2/
* freelance.de: https://www.freelance.de/Freiberufler/159128 
* [Toptal](https://toptal.com): Ich bin ein Toptal Engineer
* [Karat](https://karat.io): Ich bin ein Karat Expert Interview Engineer

## Netzwerke mit technischem Fokus

* Stackoverflow: https://stackoverflow.com/users/85615/daniel-kullmann
* Unix/Linux Stackexchange: https://unix.stackexchange.com/users/10788/daniel-kullmann
* Github: https://github.com/daniel-kullmann/
* Gitlab:  https://gitlab.com/danielkullmann/
